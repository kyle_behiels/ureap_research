\documentclass[11pt, conference]{IEEEtran}
\usepackage{authblk}
\usepackage{graphicx}
\usepackage{amsmath}
\graphicspath{{./images/}}
\newcommand{\etal}{\textit{et al}.}

\title{Analysis of Vancouver Crime and Census Data Using Various Machine Learning Algorithms}

\author{Kyle Behiels}
\affil{Department of Computer Science, \\ Thompson Rivers University \authorcr Email: kylejamesbehiels@gmail.com}


\author{Andrew Park}
\affil{Department of Computer Science, \\ Thompson Rivers University \authorcr Email: apark@tru.ca}

\usepackage[utf8]{inputenc}

\begin{document}
    \maketitle
    \begin{abstract}
        In recent years mass storage of criminal data has become a common practice for many law enforcement agencies around the world. As these data sets grow, so too does the amount of potential knowledge that we can gain from their analysis. When combined with census data the open crime data set of Vancouver, consisting of non violent crimes from between 2003 and 2019, becomes a detailed spatio-temporal snapshot of crime rate and causation. The resulting data lends itself extremely well to the application of various machine learning algorithms. In this paper we analyze the effectiveness of various machine learning algorithms as well as the attributes used as inputs.
    \end{abstract}
    \section{Background}

    Applying machine learning to crime data is not a new concept. Many studies have shown the effectiveness of analyzing crime data using various algorithms\cite{wang2013learning, mcclendon}. While the existing literature proves the effectiveness of machine learning in crime analysis, often it lacks any explanation regarding attribute selection. By combining the analysis of both attributes and algorithms we are able to gain a deeper understanding of what factors correlate to high crime rates.

    McClendon and Meghanathan \cite{mcclendon} studied the effectiveness of three machine learning algorithms; Linear Regression, Additive Regression and Decision Stump. They were able to attain a relatively strong correlation coefficient \( \rho \) for murder, rape, robbery and assault rate (\( 0.64 < \rho < 0.95\)). Their study yields surprisingly good correlation coefficients for linear regression algorithms (\( 0.73 < \rho < 0.95\)). Such high correlations for this algorithm suggest that one or more of the input attributes share a highly linear relationship with the predicted value. If this is the case, then there is likely either a very small set of high quality predictive features or a very small set of data that is redundant in regards to the goal feature.

    Another approach, presented by Wang, Rudin, Wagner and Sevieri \cite{wang2013learning} suggests analyzing patterns of individual offenders or organized groups using an algorithm they developed called "series finder". Series finder analyzes crime data and generates a tree like structure that represents various patterns in the data. Their analysis suggests that criminals tend to exhibit repeat behaviour and that data-driven tools are an effective means for detecting it. Attribute similarity algorithms are defined and, based on the similarity of two crimes, a similarity value between 0 and 1 is assigned. Identifying repeat offenders contributes to predictive policing in a way that is substantially different than our multi-modal data-based method \cite{kang2017prediction}. Their technique is attempting to identify unique groups or indivduals while our analysis involves the examination of crime trends in aggregate.

    Multi-modal spacio-temporal data was the focus of Kang and Kang in their study \cite{kang2017prediction}. They also introduced environmental context information in order to achieve better accuracy in their predictive algorithms. By seperating their data sets and evaluating them seperately, seperate algorithms could be used for generating the final predictive model. The first data set, consisting of spacial and temporal attributes, was analyzed using a multivariate poisson regression model. The second data set, consisting of pictures pulled from Google maps, was used to provide environmental context. Google maps images were evaluated using image classification deep neural networks. Once the data sets were combined into a final deep neural network, the accuracy of the resulting predictions was exceptional. Unfortunately, due to the nature of deep neural networks, it is very difficult to tell exactly how the predictions were made.

    

    \section{Machine Learning}

    \subsection{Overview}

    Data aggregation has rapidly accelerated over time as more organizations adopt the "store the data now, analyze it later" mindset. As the size of these data sets grow, so too does the need to analyze them. Machine learning (ML), a subset of artificial intelligence, gives researchers options beyond standard data analysis techniques like visualization or descriptive statistics. There are two generally accepted categories of machine learning, supervised and unsupervised. Supervised learning involves providing an algorithm with a data set that contains manually labelled data with the goal of teaching the algorithm to label the data automatically \cite{kotsiantis2007supervised}. Supervised learning is sometimes called classification learning. Conversely unsupervised learning or "clustering" uses data that has no label attribute and instead tries to derive patterns on its own. \cite{kotsiantis2007supervised} In our case, our data is labelled in that we have a pre-calculated attribute that we are attempting to predict; crimes per capita. For this reason all of the algorithms we present are supervised. 

    The Waikato Environment for Knowledge Analysis (WEKA) is a tool developed by researchers at Waikato University \cite{hall2009weka}. WEKA contains a host of knowledge analysis tools and gives researchers the capability, among other things, to perform preprocessing, data reduction, machine learning and algorithm evaluation. All machine learning algorithms presented in this paper are the WEKA implementations.

    \section{Methodology}

    \subsection{Manual Attribute Selection}

    Our dataset contains predictor data pulled mostly from Vancouvers open census \cite{vancouverOpenData}. The census, taken every five years, contains between ~1250 and ~5600 different high fidelity attributes depending on the census year. The low consistency of these data sets meant that a large amount of manual processing was necessary. Granularity of the census data sets was highly variable and occasionally contained redundant data that could have been attained by combining other attributes. Take, for example, this attribute from the 2016 census;

    \begin{quote}
        \textit{
            Language used most often at work for females in private households aged 15 years and over who worked since January 1, 2015
        }
    \end{quote}

    Furthermore, for this data attribute, there were 268 sub attributes that were they themselves categorized an additional one or two levels. Attributes like these made it necessary to perform a degree of manual attribute selection.

    Manual attribute selection was based off a set of criminology based theories, namely social disorganization theory \cite{kubrin2003new}, life-course and developmental theories \cite{farrington2017integrated} and environmental criminology theory \cite{brantingham1981environmental}.

    \subsection{Attribute Selection by Evaluation}

    After manual selection, attributes were evaluated further by assigning each an evaluation correlation coefficient \( \rho_e \) based on the nature of their relation to crimes per capita. This way we are able to evaluate both linear and non-linear relationships, something that many traditional attribute evaluation methods fail to do\cite{kang2017prediction}. \( \rho_e \) is defined as follows;

    Let
    
    \[
            \rho_l = \rho(Linear Interpolation)
    \]
    \[
        \rho_{nl} = \rho(Non Linear Interpolation)
    \]
    \[
        \rho_e = max\{|\rho_l|, |\rho_{nl}|\}
    \]

    For non-linear interpolation we used scipy's "curve\_fit" implementation with a sigmoid function as the model function. The sigmoid function is defined as follows;

    \[
        S(x) = \frac{1}{1 + e^{-x}} = \frac{e ^ x}{e^x + 1}  
    \]

    For linear interpolation we used scikit's LinearRegression implementation.

    Figure \ref{cpc_vs_male_pop} shows an example of an attribute with a highly correlated linear relationship. Note that the linear interpolation function more closely represents the dataset than the non-linear regression function.

    \begin{figure}[h]
        \centering
        \includegraphics[width=0.4\textwidth]{plot_2}
        \caption{Male Population vs. Crimes per Capita }
        \label{cpc_vs_male_pop}
    \end{figure}

    Figure \ref{cpc_vs_aboriginal}, by contrast, shows an example of an attribute with a highly correlated non-linear relationship. Note that the non-linear regression function more closely represents the dataset than the linear regression function.

    \begin{figure}[h]
        \centering
        \includegraphics[width=0.4\textwidth]{plot_20}
        \caption{Aboriginal Population vs. Crimes per Capita}
        \label{cpc_vs_aboriginal}
    \end{figure}

    \subsection{Cubic Spline Interpolation}
        
    When the municipal government of Vancouver performs a census, they divide the city into 22 census areas (see figure \ref{vancouver_census_areas}). Data is available for these areas in five year increments. Unlike the crime data set, which is specific to the hour, this is not sufficient for machine learning algorithms. In order to estimate the data in between five year increments we use a technique called cubic spline interpolation.  

    \begin{figure}[h]
        \centering
        \includegraphics[width=0.4\textwidth]{vancouver_census_areas.png}
        \caption{Vancouver Census Areas}
        \label{vancouver_census_areas}
    \end{figure}
    
    Cubic spline interpolation has the advantage of generating a smooth line from four points whereas linear interpolation generates a straight line between two points. The smoother line of cubic splines is better suited to population estimation \cite{cai2006estimating}.

    If we assume a data set consisting of four points, we can define the cubic spline interpolation function as follows;


    \begin{multline}
        S_i(x) = \frac{z_i (x - t_{i-1})^3} {6h_i} + \frac{z_{i-1} (t_i - x)^3}{6h_i} + \\ \left[ \frac{f(t_i) / h_i}{h_i} - \frac{z_i h_i}{6} \right] (x - t_{i-1}) + \\ \left[ \frac{f(t_{i-1})}{h_i} - \frac{z_{i-1} h_i}{6} \right](t_i - x)
    \end{multline}

    Where

    \begin{itemize}
        \item \( z_i = f^n (t_i) \) are the values of the second derivitive at the ith position
        \item \( h_i = t_i - t_{i-1} \)
        \item \( f(t_i) \) are the values of the function at the ith position
    \end{itemize}

    We can visualize the effectiveness of cubic spline interpolation by using an American median income data set as a test case. Figure \ref{spline_estimate} shows a cubic interpolation function that uses every five years as a prediction point starting in 1985. As demonstrated by figure \ref{spline_estimate}, we can see that, while the estimation is not perfect, it is accurate enough for our purposes.

    \begin{figure}[h]
        \centering
        \includegraphics[width=0.4\textwidth]{5_year_spline_estimate_and_actual.png}
        \caption{Cubit Spline Interpolation Demonstration}
        \label{spline_estimate}
    \end{figure}

    \subsection{Algorithm Evaluation}

    WEKA uses five metrics to evaluate the effectiveness of their classification algorithms, correlation coefficient, mean absolute error (MAE), root mean squared error (RMSE), relative absolute error (RAE) and root relative squared error (RRSE). While correlation coefficient is its own metric, MAE and RAE are closely related and are essentially just different representations of the same metric, with RAE being a representation of MAE in relation to the rest of the data set \cite{witten2016data}. Similarily, RMSE and RRSE are closely related in much the same way. All of the algorithm evaluation metrics below are WEKA's implementations.

    \subsection{Correlation Coefficient}

    The correlation coefficient, sometimes called Pearsons product-moment or \( \rho \) coefficient, is a measure of the strength of the linear relationship between two variables \cite{pearsoncorrelation}. If we say \(N=\) Number of Pairs of Scores, \(\sum xy = \) Sum of the products of paired scores, \( \sum x =\) Sum of x scores, \( \sum y =\) Sum of y scores, \( \sum x^2\) and \( \sum y^2 =\) Sum of squared x and y scores respectively, then \( \rho \) can be calculated as follows; 

    \[
    \rho = \frac{N \sum xy - (\sum x) (\sum y)}{(N \sum x^2 - (\sum x)^2) (N \sum y^2 - (\sum y)^2) }
    \]

    \subsection{Mean Absolute Error (MAE)}

    Every prediction made by machine learning algorithms cannot be expected to be completely accurate. It is therefore useful to have some measure of how much our algorithm is off by on average. Mean absolute error does this by representing the mean of all errors in our algorithm \cite{willmott2005advantages}. Let \( N = \) the number of (prediction, value) pairings and \( e = \) the set of differences of all (prediction, value) pairs then MAE can be calculated as follows;


    \[
    MAE = \left[ n^{-1} \sum_{i=1}^{N} |e_i| \right]   
    \]

    

    \subsection{Root Mean Squared Error (RMSE)}

    RMSE is almost identical as MAE but in RMSE the values in \( e \) are squared. This is done to remove the sign (counting negative values the same as positive ones) so that the magnitude of the errors better influence the average error measure. While RMSE has its place, there are  still several advantages of using MAE. This is especially true when RMSE is used in the total absence of MAE \cite{willmott2005advantages} Let \( N = \) the number of (prediction, value) pairings and \( e = \) the set of differences of all (prediction, value) pairs then RMSE can be calculated as follows;

    \[
    RMSE = \left[ n^{-1} \sum_{i=1}^{N} |e_i|^2 \right]^{1/2}
    \]

    

    \subsection{Relative Absolute Error (RAE)}

    While it is valuable to know how much our predictions are off by on average, it does not tell the whole story in terms of prediction accuracy if the predicted values are not normalized. A MAE that is considered poor for one range of predicted values could be exceptional for a different, wider range of predicted values. For this reason we use RAE to calculate the normalized value of MAE relative to the actual values. Let \( N = \) the number of (prediction, value) pairs, \( P = \)the set of predicted values and \( T =\) the set of target values then we can calculate RAE as follows;

    \[
    RAE = \frac{\displaystyle \sum_{i=1}^{N} |P_i - T_i|}{\displaystyle \sum_{i=1}^{N} |T_i - \bar{T}|}    
    \]

    Where \( \bar{T}\) is the mean of the target values, given as;

    \[
        \bar{T} = N^{-1} \sum_{i=i}^N T_i
    \]

    

    \subsection{Root Relative Squared Error RRSE}

    What MAE is to RAE, RRSE is to RMSE. Again it is a method by which we normalize the RRSE value against the entire set of predicted values. Let \( N = \) the number of (prediction, value) pairs, \( P = \)the set of predicted values and \( T =\) the set of target values then we can calculate RRSE as follows;


    \[
    RRSE = \left[\frac{\displaystyle \sum_{i=1}^{N} [P_i - T_i]^2}{\displaystyle \sum_{i=1}^{N} [T_i - \bar{T}]^2}  \right]^{1/2}
    \]
    Where \( \bar{T}\) is the mean of the target values, given as;

    \[
        \bar{T} = N^{-1} \sum_{i=i}^N T_i
    \]

    \section{Machine Learning Algorithms}

    All of the machine learning algorithms that we tested are WEKA's default implementations.

    \subsection{Multiple Linear Regression}

    Linear regression is a prediction method that simply models the relationship between an output value and the input attributes by adjusting a scalar value until it represents the data as well as possible. Since this algorithm is best at predicting linear relationships, we will use it as part of our benchmark, along with additive regression.  

    \subsection{Additive Regression}

    Additive regression is a meta classifier. Each iteration of this algorithm fits the model to the residuals left by the classifier on the previous iteration. Predictions are generated by summing the predictions of each classifier. By reducing the learning rate, the algorithm is able to better prevent overfitting at the cost of longer learning times \cite{all_classes_2018}.

    Since additive regression is best at predicting very simple non-linear relationships we will use it as part of the benchmark, along with multiple linear regression. 

    \subsection{Multilayer Perceptron (MLP)}

    Multilayer Perceptrons are a simple class of feed-forward artificial neural network \cite{all_classes_2018}. By default WEKA chooses the amount of hidden layers \( h \) in accordance with the following formula;

    \[
    h = (attributes + classes) / 2
    \]

    \subsection{K* Instance Based Classifier}

    K* is an instance based classifier meaning that the predicted value of an instance is based on instances that are similar to it. Unlike more traditional instance based classifiers, k* uses an entropy-based distance function to evaluate similarity \cite{all_classes_2018}.

    \subsection{M5 Rules}

    The M5 rules algorithm generates a decision list for regression problems. Each iteration of the algorithm builds a model tree using M5 and uses the best leaf as a rule. The resulting decision tree is used to make predictions \cite{Holmes1999,Quinlan1992,Wang1997}.

    \section{Results \& Discussion}

    The attributes that we ended up selecting based on the values shown in table \ref{evaluation_correlation_coefficients} are defined as follows;

    \begin{itemize}
        \item \textbf{median\_family\_income} - Median family income, adjusted for inflation
        \item \textbf{not\_in\_labour\_force} - Percentage of individuals not in the labour force
        \item \textbf{non\_official\_languages} - Percentage of individuals that speak a non-official language (French or English)
        \item \textbf{immigrant} - Percentage of individuals with Canadian citizenship whose country of origin is not Canada
        \item \textbf{non\_canadian} - Percentage of individuals living in the census area who are not Canadian Citizens
        \item \textbf{visible\_minority} - Percentage of individuals who are a visually identifiable minority
        \item \textbf{full\_time\_employment} - Percentage of employed individuals working full time 
        \item \textbf{college\_cert} - Percentage of individuals who have achieved some degree at the collegiate level
        \item \textbf{high\_school\_grad} - Percentage of individuals who have a high school diploma
        \item \textbf{married} - Percentage of individuals who are married
        \item \textbf{male\_pop} - Percentage of the population that is male
        \item \textbf{Males\_20\_24} - Percentage of the population that is male between 20 and 24 years old
        \item \textbf{Males\_25\_29} - Percentage of the population that is male between 25 and 29 years old
        \item \textbf{Females\_20\_24} - Percentage of the population that is female between 20 and 24 years old
        \item \textbf{Females\_25\_29} - Percentage of the population that is female between 25 and 29 years old
        \item \textbf{Mover} - Percentage of the population that has moved homes from a year prior to the census to the census date 
        \item \textbf{aboriginal} - Percentage of the population with aboriginal status
        \item \textbf{unemployed\_(LF)} - Percentage of the population in the labour force who are unemployed
        \item \textbf{trades\_diploma} - Percentage of the population with trades certification
        \item \textbf{university\_cert} - Percentage of individuals who have achieved some degree at the university level
        \item \textbf{single} - Percentage of individuals not in a relationship

    \end{itemize}

    \subsection{Selected Attributes}

    \begin{table}[htbp]
        \caption{Evaluation Coefficients for all Selected Attributes}
        \begin{tabular}{|l|r|r|r|l|}
        \hline
        \textbf{Attribute} & \(\rho_l\) &\(\rho_{nl}\)  & \(\rho_e\)  & Type \\ \hline
        median\_family\_income & -0.144 & 0.172 & 0.172 & Non-Linear \\ 
        canadian & -0.067 & 0.172 & 0.172 & Linear \\ 
        in\_labour\_force & 0.172 & 0.172 & 0.172 & Non-Linear \\ 
        not\_in\_labour\_force & -0.172 & 0.172 & 0.172 & Non-Linear \\ 
        part\_time\_employment & -0.213 & 0.172 & 0.213 & Linear \\ 
        non\_official\_languages & -0.219 & 0.172 & 0.219 & Linear \\ 
        immigrant & -0.242 & 0.172 & 0.242 & Linear \\ 
        non\_canadian & 0.066 & 0.251 & 0.251 & Non-Linear \\ 
        visible\_minority & -0.269 & 0.172 & 0.269 & Linear \\
        full\_time\_employment & 0.312 & 0.172 & 0.312 & Linear \\ 
        college\_cert & 0.322 & 0.174 & 0.322 & Linear \\ 
        high\_school\_grad & -0.327 & 0.172 & 0.327 & Linear \\
        married & -0.528 & 0.172 & 0.528 & Linear \\ 
        male\_pop & 0.638 & 0.172 & 0.638 & Linear \\
        female\_pop & -0.639 & 0.172 & 0.639 & Linear \\ 
        non\_mover & -0.663 & 0.172 & 0.663 & Linear \\ 
        Males\_20\_24 & -0.157 & 0.670 & 0.670 & Non-Linear \\ 
        Females\_20\_24 & 0.070 & 0.794 & 0.794 & Non-Linear \\ 
        Males\_25\_29 & 0.519 & 0.796 & 0.796 & Non-Linear \\ 
        mover & 0.664 & 0.831 & 0.831 & Non-Linear \\ 
        non\_immigrant & 0.194 & 0.886 & 0.886 & Non-Linear \\ 
        aboriginal & 0.328 & 0.904 & 0.904 & Non-Linear \\ 
        unemployed\_(LF) & 0.334 & 0.996 & 0.996 & Non-Linear \\ 
        trades\_diploma & 0.283 & 0.996 & 0.996 & Non-Linear \\ 
        university\_cert & 0.175 & 0.998 & 0.998 & Non-Linear \\ 
        Females\_25\_29 & 0.419 & 0.999 & 0.999 & Non-Linear \\ 
        single & 0.611 & 0.999 & 0.999 & Non-Linear \\ \hline
        \end{tabular}
        \label{evaluation_correlation_coefficients}
        \end{table}

    Table \ref{evaluation_correlation_coefficients} contains all evaluation coefficients for the attributes that were ultimately selected. Note that all population attributes are in relation to the total population that answered the census and all income statistics have been adjusted for inflation.

    \subsection{Interpreting \(\rho_l\), \(\rho_{nl}\), \(\rho_e\) }

    Recall that we define our evaluation correlation coefficient \( \rho_e \) as \( \rho_e = max\{|\rho_l|, |\rho_{nl}|\} \). Attributes with a high \( \rho_e \) value provide our machine learning algorithms with more data than those with low \( \rho_e \) values and are therefore considered more valuable predictors. Since both positive and negative correlations are equally good predictors, we evaluate the quality of an attribute based on the absolute value of their correlation coefficient.

    Attributes that are associated with the type "Linear" express a stronger correlation coefficient with the linear regression algorithm than the non-linear algorithm. This means that the relationship they have with crimes per capita is a linear one. For an example see figure \ref{cpc_vs_male_pop}.

    Conversely, attributes that are labelled "Non-Linear" are those that expressed a stronger correlation coffecient when compared to the non-linear sigmoid algorithm. For an example see figure \ref{cpc_vs_aboriginal}.

    If the value that ends up being chosen for \( \rho_e \) for a given attribute was originally a negative correlation coefficient, then that attribute is negatively correlated with crimes per capita. Most of the time this suggests an attribute that contributes to lower crime rates. For example, the negative correlation with the highest \( \rho_e \) value is non mover percentage. This suggests that populations with stable housing correlate to lower crime rates, a conclusion that is supported by criminology literature \cite{crutchfield1982crime, schmid1960urban, kelly2000inequality}. 

    Naturally the inverse holds true for attributes with high, positive correlation coefficients that were selected for \( \rho_e \). For the purpose of demonstration, the positive correlation with the highest \( \rho_e \) that is also supported by ample criminology literature is unemployed individuals in labour force percentage (unemployed\_(LF)). This suggests that indivduals seeking work who cannot find it have a tendency to commit crimes at a higher rate. Again we can find a fair amount of support for this conclusion \cite{raphael2001identifying, farrington1986unemployment, edmark2005unemployment}.

    \subsection{Attributes with Negative Correlation}

    Using \( \rho_e \) as a measure of predictive strength for each attribute and the sign as an indicator of its relationship relative to crimes per capita, attributes that contribute to lower crime rates can be seen in table \ref{low_crime_attribs}. Non mover percentage is the strongest negative correlation and part time employment is the weakest.

    \begin{table}[htbp]
        \caption{Attributes that Correlate with Low Crime Rates}
        \centering
        \begin{tabular}{|l|r|l|}
        \hline
            \textbf{Attribute} & \( \rho_e \) & \textbf{type} \\ \hline 
            non\_mover & 0.663 & Linear \\ 
            female\_pop & 0.639 & Linear \\ 
            married & 0.528 & Linear \\ 
            high\_school\_grad & 0.327 & Linear \\ 
            visible\_minority & 0.269 & Linear \\ 
            immigrant & 0.242 & Linear \\ 
            non\_official\_languages & 0.219 & Linear \\
            part\_time\_employment & 0.213 & Linear \\ \hline
        \end{tabular}
        \label{low_crime_attribs}
    \end{table}

    \subsection{Attributes with Positive Correlation}

    Intuitively we can do the same for attributes that correlate to high crime rates. The results can be seen in table \ref{high_crime_attribs}. The strongest positive correlation is single population percentage and the weakest is median family income.

    \begin{table}[htbp]
        \caption{Attributes that Correlate with High Crime Rates}
        \centering
        \begin{tabular}{|l|r|l|}
        \hline
            \textbf{Attribute} & \( \rho_e \) & \textbf{type} \\ \hline 
            single & 0.999 & Non-Linear \\ 
            Females\_25\_29 & 0.999 & Non-Linear \\
            university\_cert & 0.998 & Non-Linear \\ 
            trades\_diploma & 0.996 & Non-Linear \\ 
            in\_labour\_force\_unemployed & 0.996 & Non-Linear \\ 
            aboriginal & 0.904 & Non-Linear \\ 
            non\_immigrant & 0.886 & Non-Linear \\
            mover & 0.831 & Non-Linear \\ 
            Males\_25\_29 & 0.796 & Non-Linear \\
            Females\_20\_24 & 0.794 & Non-Linear \\ 
            Males\_20\_24 & 0.670 & Non-Linear \\ 
            male\_pop & 0.638 & Linear \\ 
            college\_cert & 0.322 & Linear \\ 
            full\_time\_employment & 0.312 & Linear \\ 
            non\_canadian & 0.251 & Non-Linear \\ 
            not\_in\_labour\_force & 0.172 & Non-Linear \\
            in\_labour\_force & 0.172 & Non-Linear \\ 
            canadian & 0.172 & Linear \\ 
            median\_family\_income & 0.172 & Non-Linear \\ \hline
        \end{tabular}
        \label{high_crime_attribs}
    \end{table}

    While some of the attributes make sense from a criminology perspective, others seem counter-intuitive. in\_labour\_force\_unemployed and non\_mover for example both correlate in a way that makes sense. In contrast, university graduates having a high positive correlation with crime rate seems to contradict conventional wisdom \cite{ehrlich1975relation, lochner2007education}. This could be due to complex, underlying relationships that are too complicated to make sense on their own, or it could be due to some bias that exists solely in the Vancouver area.

    \subsection{Algorithm Results}

    All ML algorithms are run using 10-fold cross validation. As mentioned above, the performance of WEKAs algorithms and our attributes is evaluated using the \( \rho \), MAE, RMSE, RAE and RRSE values. 
    
    Multiple linear regression algorithms are most effective when evaluating purely linear relationships. Similarily, non-linear regression algorithms are most effective when evaluating very simple non-linear regression relationships. For these reasons we will be using WEKA's multiple linear regression algorithm (MLR) and additive regression algorithm (AR) as benchmarks. It is important to note that the value for crimes per capita is not normalized and ranges from 0 to 0.15.

    The three other algorithms that we tested are multi-layer perceptron (MP), K* and M5 Rules. All performed better than both MLR and AR in all evaluation metrics as shown in table \ref{algorithm_results}. 

    \begin{table}[htbp]
        \caption{Algorithm Performance Evaluation}
        \begin{tabular}{|l|r|r|r|r|r|}
        \hline 
        \textbf{Algorithm} & \( \rho \) & \textbf{MAE} & \textbf{RMSE} & \textbf{RAE} & \textbf{RRSE} \\
        \hline
        K* & 0.9632 & 0.0025 & 0.004 & 27.1469 & 26.9031 \\ 
        M5 Rules & 0.9546 & 0.0027 & 0.0044 & 29.7192 & 29.7935 \\ 
        MP & 0.954 & 0.0029 & 0.0045 & 32.076 & 30.2443 \\ \hline
        AR & 0.9248 & 0.0038 & 0.0057 & 41.7708 & 38.2911 \\
        MLR & 0.9131 & 0.004 & 0.006 & 44.3656 & 40.7695 \\ \hline
        \end{tabular}
        \label{algorithm_results}
    \end{table}

    The high performance of these algorithms in relation to AR and MLR suggests that they are able to make predictions based on relationships in the data that are more complex than just linear and non-linear. By evaluating our attributes using both linear and non-linear relations, we are able to achieve a higher degree of accuracy than either method alone.

    \section{Conclusion}

    By developing not only a method for attribute selection, but also a means by which to evaluate the selected attributes, we are able to verify the efficacy of a given data set at predicting crime rates. Now that we are able to reliably produce results using this technique, we can begin to analyze data sets from other cities around the world. As patterns begin to emerge in these results, we can gain a better understanding of not only what factors lead to high crime universally around the world, but also how contributing factors differ from place to place.

    By better understanding what societal factors lead to crime, criminologists and law enforcement professionals alike can gain insight into its prevention. Having the ability to verify these societal factors is an invaluable tool that can be used to study crime across the globe.

    \bibliography{mybib}
    \bibliographystyle{ieeetr}

    
\end{document}