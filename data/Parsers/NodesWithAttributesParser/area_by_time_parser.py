import csv
from scipy.interpolate import interp1d
import json

CRIMES_TO_EXCLUDE = ["Vehicle Collision or Pedestrian Struck (with Injury)", "Vehicle Collision or Pedestrian Struck (with Fatality)"]
MIN_YEAR = 2003
MAX_YEAR = 2015

def main():
    census_data = getData()[0]
    census_fields = getData()[1]

    area_table = {}

    localAreasData = open("cov_localareas.csv", "r")
    localAreasReader = csv.reader(localAreasData)

    next(localAreasReader)

    for row in localAreasReader:
        area_table[row[0]] = str(row[1])

    # [area][year]
    yearly_area_crime_counts = {}
    monthly_area_crime_counts = {}

    crime_data_stream = open("counted_nodes.json", 'r')

    crime_data = json.loads(crime_data_stream.read())

    for kernel in crime_data["kernels"]:
        area = kernel["area"]

        for month in kernel["crimes"]["monthly"]:
            monthly_total = 0
            for crime_type in kernel["crimes"]["monthly"][month]:
                if(crime_type in CRIMES_TO_EXCLUDE):
                    continue
                monthly_total += kernel["crimes"]["monthly"][month][crime_type]
                try:
                    monthly_area_crime_counts[area][month] += monthly_total

                except:
                    try:
                        monthly_area_crime_counts[area][month] = monthly_total
                    except:
                        monthly_area_crime_counts[area] = {}
                        monthly_area_crime_counts[area][month] = monthly_total

        for year in kernel["crimes"]["yearly"]:
            yearly_total = 0
            for crime_type in kernel["crimes"]["yearly"][year]:
                if(crime_type in CRIMES_TO_EXCLUDE):
                    continue
                yearly_total += kernel["crimes"]["yearly"][year][crime_type]
                try:
                    yearly_area_crime_counts[area][year] += yearly_total

                except:
                    try:
                        yearly_area_crime_counts[area][year] = yearly_total
                    except:
                        yearly_area_crime_counts[area] = {}
                        yearly_area_crime_counts[area][year] = yearly_total


    # CRIME_TOTAL, AREA, YEAR, REST_OF_DATA

    final_writeable = [[]]
    final_writeable_monthly = [[]]

    print(yearly_area_crime_counts)

    for year in range(MIN_YEAR, MAX_YEAR+1):

        for month in range(1,13):
            prepend = ''
            if (month < 10):
                prepend = "0"

            yearMonthKey = str(year) + "-" + prepend + str(month)
            for area in monthly_area_crime_counts:
                area_month_data = []

                area_month_data.append(monthly_area_crime_counts[area][yearMonthKey])

                area_month_data.append(area)

                area_month_data.append(yearMonthKey)

                for field in census_fields:
                    if(field == 'area'):
                        continue
                    successful_reads = 0
                    data_total = 0

                    try:
                        data_total += getEstimatedData(field, area_table[area], year, month, census_data, census_fields)
                        successful_reads += 1
                    except:
                        pass
                    if(not successful_reads == 0):
                        area_month_data.append(float(data_total) / float(successful_reads))
                    else:
                        area_month_data.append(None)
                final_writeable_monthly.append(area_month_data)

        for area in yearly_area_crime_counts:
            area_year_data = []

            # Append crime count as first field 
            area_year_data.append(yearly_area_crime_counts[area][str(year)])

            # Append area as second field
            area_year_data.append(area)
            # Year as third field
            area_year_data.append(year)

            for field in census_fields:
                if(field == 'area'):
                    continue 
                successful_reads = 0
                data_total = 0
                for month in range(0,12):
                    try:
                        data_total += getEstimatedData(field, area_table[area] , year, month, census_data, census_fields)
                        successful_reads += 1
                    except:
                        pass
                    
                if(not successful_reads == 0):
                    area_year_data.append(float(data_total) / float(successful_reads))
                else:
                    area_year_data.append(None)

            final_writeable.append(area_year_data)

    output_file_identifier = ""

    if(CRIMES_TO_EXCLUDE):
        output_file_identifier = "_excludes"
        for crime in CRIMES_TO_EXCLUDE:
            output_file_identifier = output_file_identifier + "_" + crime.replace(" ", "_").lower()


    outfile = open("crimes_and_census_by_year" + output_file_identifier + ".csv", "w")
    outfile_monthly = open("crimes_and_census_by_month" + output_file_identifier + ".csv", "w")

    monthly_fields_copy = census_fields

    monthly_fields_copy = monthly_fields_copy[1:len(monthly_fields_copy)]

    monthly_fields_copy.insert(0, "crime_total")
    monthly_fields_copy.insert(1, "area")
    monthly_fields_copy.insert(2, "month")

    outcsv_monthly = csv.writer(outfile_monthly)

    outcsv_monthly.writerow(monthly_fields_copy)
    outcsv_monthly.writerows(final_writeable_monthly)


    census_fields.insert(0, "crime_total")
    census_fields.insert(1, "area")
    census_fields.insert(2, "year")

    outcsv = csv.writer(outfile)

    print(final_writeable)

    outcsv.writerow(census_fields)
    outcsv.writerows(final_writeable)

                        
def getAreas(): 

    returnable = []

    open_file = open("census_data_2001.csv", "r")

    csvReader = csv.reader(open_file)

    next(csvReader)

    for row in csvReader:
        returnable.append(row[0])
    
    return returnable


def getData():
    fields = []

    open_files = {}

    open_files["2001"] = open("census_data_2001.csv", "r")
    open_files["2006"] = open("census_data_2006.csv", "r")
    open_files["2011"] = open("census_data_2011.csv", "r")
    open_files["2016"] = open("census_data_2016.csv", "r")

    csv_readers = {}

    for _file in open_files:
        csv_readers[_file] = csv.reader(open_files[_file])

    data = {}

    for reader in csv_readers:

        fields = next(csv_readers[reader])
        rows = []
        for row in csv_readers[reader]:
            rows.append(row)
        data[reader] = rows
    
    return [data, fields]

def getEstimatedData(field, area, year, month, data, fields):

    # Hardcoded min and max dates
    min_year = 2001
    max_year = 2015

    indexOfField = fields.index(field)

    index = 0
    interp_x = []
    interp_y = []
    for data_year in data:
        interp_x.append(int(data_year))
        for row in data[data_year]:
            if(row[indexOfField] == '*' or row[indexOfField] == ''):
                return None
            elif(row[0] == area):
                interp_y.append(row[indexOfField].replace('$', '').replace(',', ''))
                continue

    f = interp1d(interp_x, interp_y, kind='cubic')


    return f(float(year) + (month/12))[()]

main()
