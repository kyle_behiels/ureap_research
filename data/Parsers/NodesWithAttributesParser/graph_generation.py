import matplotlib.pyplot as plt
import json, csv
import numpy as np
import sklearn
from sklearn.linear_model import LinearRegression
from scipy.optimize import curve_fit, least_squares, differential_evolution
from scipy import stats, asarray as ar, exp
from lmfit import Model
import math



def obj_func(x, a, b, c):
    
    return a + b*x + c*x*x


# a = a
# x = x
# x0 = b
# c = sigma
def gaus(x,a,x0,sigma, offset):
    return a*exp(-(x-x0)**2/(2*sigma**2)) + offset

def main():

    final_data = open("reduced_monthly_census_no_car_accidents.csv", "r")

    final_data_reader = csv.reader(final_data)

    fields = next(final_data_reader)

    rows = []

    visData = {}

    for row in final_data_reader:
        rows.append(row)

    for row in rows:
        for point in row:
            if(row.index(point) == 0 or row.index(point) == 1):
                continue
            try:
                visData[row.index(point)].append((point, row[1]))
            except:
                visData[row.index(point)] = []
                visData[row.index(point)].append((point, row[1]))


    plots = []


    plt.clf()


    for attribute_set in visData:
        plots_x = []
        plots_y = []
        for point in visData[attribute_set]:
            if(float(point[0]) == 0):
                point[0] = 0.0001

            if(float(point[1]) == 0):
                point[1] = 0.0001
            plots_x.append(float(point[0]))
            plots_y.append(float(point[1]))

        # plots_x_train = np.array(plots_x)
        # plots_y_train = np.array(plots_y)

        # reg = LinearRegression().fit(plots_x_train[:, np.newaxis], plots_y_train[:, np.newaxis])

        # reg_y = reg.predict(plots_x)


        plt.scatter(plots_x, plots_y)
        # plt.plot(plots_x, reg_y)

        x_train = np.array(plots_x).reshape((-1,1))
        y_train = np.array(plots_y)

        x0 = np.ones(len(plots_x))
        sigma = x0


        non_lin_x = ar(plots_x)
        non_lin_y = ar(plots_y)

        # non_lin_n = len(non_lin_x)
        # non_lin_mean = sum(non_lin_x * non_lin_y)/non_lin_n 
        # non_lin_sigma = sum(non_lin_y*(non_lin_x - non_lin_mean)**2)/non_lin_n


        # popt,pcov = curve_fit(gaus,non_lin_x,non_lin_y, maxfev=(len(non_lin_x) * 1000),  p0=[np.max(non_lin_y), 
        #                                                                                      np.median(non_lin_x), 
        #                                                                                      np.std(non_lin_x), 
        #                                                                                      np.min(non_lin_y)])

        # Generate genetic predictions
        # ==================================================================================================

        # Non linear regression support functions

        def  siga_offset(x, a, b, Offset):
            return 1.0 / (1.0 + np.exp(-a * (x-b))) + Offset

        def sumOfSquaredError(parameterTuple):
            # warnings.filterwarnings("ignore") # do not print warnings by genetic algorithm
            val = obj_func(non_lin_x, *parameterTuple)
            return np.sum((non_lin_y - val) ** 2.0)

        def generate_Initial_Parameters():
            # min and max used for bounds
            maxX = max(non_lin_x)
            minX = min(non_lin_x)
            maxY = max(non_lin_y)
            minY = min(non_lin_y)

            parameterBounds = []
            parameterBounds.append([minX, maxX]) # seach bounds for a
            parameterBounds.append([minX, maxX]) # seach bounds for b
            parameterBounds.append([0.0, maxY]) # seach bounds for Offset

            # "seed" the numpy random number generator for repeatable results
            result = differential_evolution(sumOfSquaredError, parameterBounds, seed=3)
            return result.x

        # End of non linear regression support functions

        geneticParameters = generate_Initial_Parameters()

        fittedParameters, pcov = curve_fit(siga_offset, non_lin_x, non_lin_y, geneticParameters, maxfev=10000)

        print('Parameters ', geneticParameters)

        modelPredictions = siga_offset(non_lin_x, *fittedParameters)
        absError = modelPredictions - non_lin_y

        # ===================================================================================================

        # Graph genetic predictions
        # ===================================================================================================

        xModel = np.linspace(min(non_lin_x), max(non_lin_x), 100)
        yModel = siga_offset(xModel, *fittedParameters)

        # Define the font for the graphs
        font={'family': 'normal',
            'weight': 'normal',
            'size' : 16}
        
        plt.rc('font', **font)


        plt.plot(xModel, yModel, 'k--', 
                                label='Non-Linear Regression \n\u03C1 = ' + str('{0:.3f}'.format(stats.pearsonr(xModel, yModel)[0])))


        model = LinearRegression()
        model.fit(x_train, y_train)

        predict_data = np.column_stack((x_train.reshape((1,-1))[0], y_train ))

        regression_y = model.predict(x_train)

        print("============================================================\nPlot # ", str(attribute_set), '\n============================================================')
        print(regression_y)
        print("Correlation Coefficient (LR) = ", stats.pearsonr(plots_x, plots_y)[0])
        print("Correlation Coefficient (NLR) = ", stats.pearsonr(xModel, yModel)[0])

        plt.gcf().subplots_adjust(bottom=0.15, left=0.15)

        plt.plot(plots_x,regression_y, 'r', 
                                label="Linear Regression\n\u03C1 = " + str('{0:.3f}'.format(stats.pearsonr(plots_x, plots_y)[0])))

        # plt.plot(plots_x, gaus(plots_x, *popt), 'g--', label="Gaussian Regression")

        plt.yscale('linear')
        plt.xscale('linear')

        plt.legend(loc='upper center')

        plt.xlabel(fields[attribute_set], **font)
        plt.ylabel("Crimes per Capita", **font)
        
        plt.savefig("plot_"+str(attribute_set)+".png")
        plt.clf()

    

main()
