import csv, math

def main():
    data_by_month_file = open("census_monthly_excludes_car_accidents.csv", "r")
    data_by_month_reader = csv.reader(data_by_month_file)

    data_by_month_fields = next(data_by_month_reader)

    rows = []

    for row in data_by_month_reader:
        rows.append(row)

    unique_areas = []

    for row in rows:
        unique_areas.append(row[1])

    unique_areas = list(set(unique_areas))

    STR_vals = []
    SUN_vals = []

    for area in unique_areas:
        area_min = math.inf
        area_max = -math.inf
        for row in rows:
            if row[1] == area:
                if(area == "STR"):
                    STR_vals.append(float(row[3]))
                if(area == "SUN"):
                    SUN_vals.append(float(row[3]))
                if(not row[3]):
                    continue
                area_min = float(area_min)
                area_max = float(area_max)
                if(float(row[3]) < area_min):
                    area_min = row[3]
                if(float(row[3]) > area_max):
                    area_max = row[3]
        print("=============\nArea = ", area, "\nMin = ", area_min, "\nMax = ", area_max)


    print("vals for str\n============")
    for val in STR_vals:
        print(val)

    print("vals for sun\n============")
    for val in SUN_vals:
        print(val)


main()