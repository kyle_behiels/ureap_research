import json, csv
from flask import jsonify
from scipy.interpolate import interp1d
from collections import defaultdict

MIN_YEAR = 2001
MAX_YEAR = 2016

CRIMES_TO_EXCLUDE = ["Vehicle Collision or Pedestrian Struck (with Injury)", "Vehicle Collision or Pedestrian Struck (with Fatality)"]

def main():

    data = getData()[0]
    fields = getData()[1]
    
    # print(getEstimatedData('male_pop','Arbutus-Ridge', 2004, 1, data, fields))

    areas = getAreas()

    for year in data:
        data[year][0]

    info = {}

    run_tracker = 0

    for area in areas:
        print(str(run_tracker) + "/" + str(len(areas)))
        run_tracker += 1
        if(area not in info):
            info[area] = {}
        for field in fields:
            if(field == "area"):
                continue
            if(field not in info[area]):
                info[area][field] = {}
            for year in range(MIN_YEAR, MAX_YEAR):
                if(year not in info[area][field]):
                    info[area][field][year] = {}
                for month in range(1, 13):
                    if(month not in info[area][field][year]):
                        info[area][field][year][month] = {}
                    info[area][field][year][month] = getEstimatedData(field, area, year, month, data, fields)

    openfile = open("counted_nodes.json", "r")
    data_obj = json.loads(openfile.read())

    out_kernels = []

    for kernel in data_obj["kernels"]:
        newKern = kernel

    print(info)

    output = open("estimated_census.json", "w")

    writeable = json.dumps(info)

    output.write(writeable)

    # Now start reading data from crimes and combine the two

    final_csv_writeable_data_yearly = [[]]
    final_csv_writeable_data_monthly = [[]]

    crime_data = json.loads(open("counted_nodes.json", "r").read())

    # Generate a lookup table for neighborhood codes

    area_table = {}

    localAreasData = open("cov_localareas.csv", "r")
    localAreasReader = csv.reader(localAreasData)

    next(localAreasReader)

    for row in localAreasReader:
        area_table[row[0]] = str(row[1])

    # Parse the data and generate a CSV for WEKA

    # info[area][field][year][month]
    total = len(crime_data["kernels"])
    i = 0
    for kernel in crime_data["kernels"]:
        # print(str(i) + "/" + str(total))
        i += 1
        area = area_table[kernel["area"]]



        for year in range(MIN_YEAR, MAX_YEAR):
            yearly_total = 0
            if(str(year) not in kernel["crimes"]["yearly"]):
                yearly_total = 0
                continue
            for crimeType in kernel["crimes"]["yearly"][str(year)]:
                if(crimeType not in CRIMES_TO_EXCLUDE):
                    yearly_total += int(kernel["crimes"]["yearly"][str(year)][crimeType])
                else:
                    # print("Skipping " + crimeType)
                    pass

            
            node_info = []
            node_info.append(yearly_total)
            node_info.append(year)
            node_info.append(area)

            for field in fields:
                field_year_total = 0
                successful_reads = 0
                if(field == "area"):
                    continue
                for month in info[area][field][year]:
                    
                    try:
                        field_year_total += info[area][field][year][month]
                        successful_reads += 1
                    except:
                        field_year_total = None

                if(not successful_reads == 0):
                    node_info_append = float(field_year_total) / float(successful_reads)
                else:
                    node_info_append = None

                node_info.append(node_info_append)
            
            final_csv_writeable_data_yearly.append(node_info)

    output_file_identifier = ""

    if(CRIMES_TO_EXCLUDE):
        output_file_identifier = "_excludes"
        for crime in CRIMES_TO_EXCLUDE:
            output_file_identifier = output_file_identifier + "_" + crime.replace(" ", "_").lower()


    write_file = open("final_file" + output_file_identifier + ".csv", "w")

    fields.insert(0, "crime_total")
    fields.insert(1, "year")

    write_csv = csv.writer(write_file)
    write_csv.writerow(fields)
    write_csv.writerows(final_csv_writeable_data_yearly)


def getAreas():

    returnable = []

    open_file = open("census_data_2001.csv", "r")

    csvReader = csv.reader(open_file)

    next(csvReader)

    for row in csvReader:
        returnable.append(row[0])
    
    return returnable

def getData():
    fields = []

    open_files = {}

    open_files["2001"] = open("census_data_2001.csv", "r")
    open_files["2006"] = open("census_data_2006.csv", "r")
    open_files["2011"] = open("census_data_2011.csv", "r")
    open_files["2016"] = open("census_data_2016.csv", "r")

    csv_readers = {}

    for _file in open_files:
        csv_readers[_file] = csv.reader(open_files[_file])

    data = {}

    for reader in csv_readers:

        fields = next(csv_readers[reader])
        rows = []
        for row in csv_readers[reader]:
            rows.append(row)
        data[reader] = rows
    
    return [data, fields]

def getEstimatedData(field, area, year, month, data, fields):

    # Hardcoded min and max dates
    min_year = 2001
    max_year = 2015

    indexOfField = fields.index(field)

    index = 0
    interp_x = []
    interp_y = []
    for data_year in data:
        interp_x.append(int(data_year))
        for row in data[data_year]:
            if(row[indexOfField] == '*' or row[indexOfField] == ''):
                return None
            elif(row[0] == area):
                interp_y.append(row[indexOfField].replace('$', '').replace(',', ''))
                continue

    f = interp1d(interp_x, interp_y, kind='cubic')


    return f(float(year) + (month/12))[()]



main()

    

