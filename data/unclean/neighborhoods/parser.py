import csv

def main():
    datafile = open("neighborhood_polygon_coords.csv", "r")
    csvObj = csv.reader(datafile)

    fields = next(csvObj)

    rows = []

    for row in csvObj:
        rows.append(row)

    datafile.close()

    sunset_data_for_test = []

    ind = 0
    print(fields)
    for row in rows:
        if(row[0] == "SUN"):
            ind += 1
            sunset_data_for_test.append([ind, float(row[2]), float(row[3])])

    writeFile = open("sunset.csv", "w")
    writeCSV = csv.writer(writeFile)

    writeCSV.writerow(["index", "lat", "lon"])

    writeCSV.writerows(sunset_data_for_test)

main()