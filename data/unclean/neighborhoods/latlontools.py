import utm

def metersFromLatLon(point1, point2):
    """Get distance in meters (manhattan) between two WGS84 points

    The further apart the two points are, the lower the precision

    """
    distanceLat = abs(point1[0] - point2[0])
    distanceLon = abs(point1[1] - point2[1])

    distanceLatMeters = distanceLat * (111.32 * SCALE) 
    distanceLonMeters = distanceLon * (40075 * SCALE * math.cos(point1[0]) / 360)

    return math.sqrt(pow(distanceLatMeters, 2) + pow(distanceLonMeters, 2))

# Only for use within Vancouver area or anywhere that falls under the zone 10N NAD83 specification
# this is simply an abstraction method for my own sanity

def NAD83toWSG84(nad83):
    """Only for use within Vancouver area or anywhere that falls under the zone 10N NAD83 specification this is simply an abstraction method for my own sanity

    Parameters:
        nad83:
            Tuple containing the nad83 coordinates for conversion
    
    Returns:
        Tuple containing the wsg84 coordinates
    """
    return utm.to_latlon(nad83[0], nad83[1], 10, "N")