import polygon_test as pt 
import csv, math

SCALE = 500

# Kernel size in meters
KERNEL_SIZE = 50

def unique(mList):
    return list(set(mList))

def getUniquePolys():
    """Get all unique polygons from the neighborhood file
    """

    polyData = open("neighborhood_polygon_coords.csv", "r")
    polyReader = csv.reader(polyData)

    fields = next(polyReader)

    rows = []

    for row in polyReader:
        rows.append(row)

    polyData.close()

    uniqueNames = []
    for row in rows:
        uniqueNames.append(row[0])

    uniqueNames = unique(uniqueNames)

    allPolygons = []

    for uniqueName in uniqueNames:
        points = []
        for row in rows:
            if row[0] == uniqueName:
                points.append((float(row[2]), float(row[3])))
        allPolygons.append({
            "name": uniqueName,
            "points": points
        })
    
    return allPolygons

def createGrid():
    
    polygons = getUniquePolys()

    # X = LONGITUDE Y = LATITUDE

    minLat = 500.0
    minLon = 500.0
    maxLat = -500.0
    maxLon = -500.0

    for poly in polygons:
        for point in poly['points']:
            if point[0] < minLat:
                minLat = point[0]
            elif point[0] > maxLat:
                maxLat = point[0]
            elif point[1] < minLon:
                minLon = point[1]
            elif point[1] > maxLon:
                maxLon = point[1]

    print("minLat = ", minLat, "\nminLon = ", minLon, "\nmaxLat = ", maxLat, "maxLon = ", maxLon)
    print((maxLat - minLat) * 10000)
    print((maxLon - minLon) * 10000)

    print(metersFromLatLon((minLat, minLon), (maxLat, maxLon)))

    kernelPoints = []
    kernelPointsForCSV = []
    index = 0
    for poly in polygons:
        index += 1
        for x in range(0, int((maxLon - minLon) * SCALE)):
            print(index, "/", len(polygons), "Polys & ",x, "/", int((maxLon - minLon) * SCALE), "Lats")
            for y in range(0, int((maxLat - minLat) * SCALE)):
                point = (minLat + (float(y) / float(SCALE)), minLon + (float(x) / float(SCALE)))
                if(pt.pointInPolygon(point, poly["points"])):
                    kernelPoints.append({
                        "area": poly["name"],
                        "latitude": point[0],
                        "longitude": point[1]
                    })
                    kernelPointsForCSV.append([poly['name'], point[0], point[1]])
    csvFile = open("kernelGrid_" + str(SCALE) + "_scale.csv", 'w')
    csvWriter = csv.writer(csvFile)

    csvWriter.writerow(["area", "latitude", "longitude"])
    csvWriter.writerows(kernelPointsForCSV)

    csvFile.close()

createGrid()