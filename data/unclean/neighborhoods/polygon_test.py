import csv

"""Module for checking if a point exists inside of a polygon

This modules serves to check if a point exists inside of a polygon. Polygons 
must be defined as a ordered set of points. Each point is a tuple of length
two and can an integer, double or float.

"""

# CODE BORROWED FROM -> https://stackoverflow.com/a/18524383

# Hard Coded for Efficiency
PRECISION = 10000000000

# These values are placeholders that get overridden. Useful for large scales where efficiency is important.
minLonPoly = -123.224845886 
minLatPoly = 49.1989364624
maxLonPoly = -123.023200989
maxLatPoly = 49.2958106995

def side(a,b,c):
    """ Returns a position of the point c relative to the line going through a and b
        Points a, b are expected to be different
    """
    d = (c[1]-a[1])*(b[0]-a[0]) - (b[1]-a[1])*(c[0]-a[0])
    return 1 if d > 0 else (-1 if d < 0 else 0)

def is_point_in_closed_segment(a, b, c):
    """ Returns True if c is inside closed segment, False otherwise.
        a, b, c are expected to be collinear
    """
    if a[0] < b[0]:
        return a[0] <= c[0] and c[0] <= b[0]
    if b[0] < a[0]:
        return b[0] <= c[0] and c[0] <= a[0]

    if a[1] < b[1]:
        return a[1] <= c[1] and c[1] <= b[1]
    if b[1] < a[1]:
        return b[1] <= c[1] and c[1] <= a[1]

    return a[0] == c[0] and a[1] == c[1]

#
def lines_intersect(a,b,c,d):
    """ Verifies if closed segments a, b, c, d do intersect.
    """
    if a == b:
        return a == c or a == d
    if c == d:
        return c == a or c == b

    s1 = side(a,b,c)
    s2 = side(a,b,d)

    # All points are collinear
    if s1 == 0 and s2 == 0:
        return \
            is_point_in_closed_segment(a, b, c) or is_point_in_closed_segment(a, b, d) or \
            is_point_in_closed_segment(c, d, a) or is_point_in_closed_segment(c, d, b)

    # No touching and on the same side
    if s1 and s1 == s2:
        return False

    s1 = side(c,d,a)
    s2 = side(c,d,b)

    # No touching and on the same side
    if s1 and s1 == s2:
        return False

    return True

# END OF BORROWED CODE


def pointInPolygon(point, polygon):
    """ Checks if a given point is within a given polygon

    Parameters:
        point (float lat, float lon): A point made of x,y coordinates
        polygon [(float lat1, float lon1)...(float latn, float lonn)]: List of ordered points that make up the edge of a polygon

    Returns:
        pointInPolygon(Bool): True if the point is inside the polygon, else false

    """

    # We need lats and longs so that min and max can be used to discretize the WSG84 coords
    
    polyLons = []
    polyLats = []

    PRECISION = 0

    for p in polygon:
        polyLons.append(p[0])
        polyLats.append(p[1])
        if(len(str(p[0]).split('.')[1]) > PRECISION):
            PRECISION = len(str(p[0]).split('.')[1])

        if(len(str(p[1]).split('.')[1]) > PRECISION):
            PRECISION = len(str(p[1]).split('.')[1])

    PRECISION = pow(10, PRECISION)

    minLonPoly = min(polyLons)
    minLatPoly = min(polyLats)
    maxLonPoly = max(polyLons)
    maxLatPoly = max(polyLats)

    # Define a line that will be used for the Ray Cast algorithm (horizontal)

    lineStart = (point[0] * PRECISION, point[1] * PRECISION)   
    lineEnd = (int((maxLonPoly * PRECISION) * 2), int(point[1] * PRECISION))

    ray = (lineStart, lineEnd)

    # Check how many times that line crosses the polygon

    intersectCount = 0

    if(point in polygon):
         return True # Edge case where the point lies on a vertex of the polygon

    for i in range(0, len(polygon)):
        polyLineEnd = (0,0)
        polyLineStart = ( int(polygon[i][0] * PRECISION), int(polygon[i][1] * PRECISION) )
        try:
            polyLineEnd = ( int(polygon[i+1][0] * PRECISION), int(polygon[i+1][1] * PRECISION))

            if(point[0] == polygon[i][0]):  # The x coords are the same
                if((point[1] > polygon[i][1] and point[1] < polygon[i+1][1]) or (point[1] < polygon[i][1] and point[1] > polygon[i+1][1])): #The point is on the polygons perimeter
                    return True
            if(point[1] == polygon[i][1]): # The y coords are the same
                if((point[0] > polygon[i][0] and point[0] < polygon[i+1][0]) or (point[0] < polygon[i][0] and point[0] > polygon[i+1][0])): #The point is on the polygons perimeter
                    return True

        except:
            polyLineEnd = ( int(polygon[0][0] * PRECISION), int(polygon[0][1] * PRECISION)  )

            if(point[0] == polygon[i][0]):  # The x coords are the same
                if((point[1] > polygon[i][1] and point[1] < polygon[0][1]) or (point[1] < polygon[i][1] and point[1] > polygon[0][1])): #The point is on the polygons perimeter
                    return True
            if(point[1] == polygon[i][1]): # The y coords are the same
                if((point[0] > polygon[i][0] and point[0] < polygon[0][0]) or (point[0] < polygon[i][0] and point[0] > polygon[0][0])): #The point is on the polygons perimeter
                    return True

        if(lines_intersect(lineStart, lineEnd, polyLineStart, polyLineEnd)):
            intersectCount += 1
            
    if(intersectCount % 2 == 0):
        return False
    
    return True


