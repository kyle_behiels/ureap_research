import utm
import json
import sys
import random
import multiprocessing
import csv
from multiprocessing.pool import ThreadPool

def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        bar_length  - Optional  : character length of bar (Int)
    """
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '█' * filled_length + '-' * (bar_length - filled_length)

    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()


def countCrime(allStreetCoords):

    crimeDataFile = open("crimeDataExtended.csv", "r")
    crimeDataCsv = csv.reader(crimeDataFile)

    fields = next(crimeDataCsv)

    crime_rows = []

    for row in crimeDataCsv:
        crime_rows.append(row)

    streetCrimeCounts = [0] * len(allStreetCoords)

    totalCrimes = 0
    
    for row in crime_rows:
        totalCrimes += 1

    index = 0
    print_progress(index, totalCrimes, "Counting Crimes")
    for row in crime_rows:
        print_progress(index, totalCrimes, "Counting Crimes" )
        curClosest = 10000
        streetIndex = 0
        curClosestIndex = 0
        if index != 0:
            crime_lat = float(row[8])
            crime_lon = float(row[9])
            for street in allStreetCoords:
                for coord in street:
                    # print(coord)
                    if(len(coord) != 0):
                        distance = abs(crime_lat - coord[0]) + abs(crime_lon - coord[1])
                        if(distance < curClosest):
                            # print(distance)
                            curClosest = distance
                            curClosestIndex = streetIndex
                streetIndex+=1
            streetCrimeCounts[curClosestIndex] += 1
        index += 1
    return streetCrimeCounts
    
def countGraffiti(allStreetCoords):
    graffitiFile = open("graffiti.csv", "r")
    totalGraffitiCount = 0
    for line in graffitiFile:
        totalGraffitiCount += 1

    graffitiFile.close()
    graffitiFile = open("graffiti.csv", "r")
    streetGrafCounts = [0] * len(allStreetCoords)
    index = 0
    for line in graffitiFile:
        print_progress(index, totalGraffitiCount, "Counting Graffiti")
        curClosest = 10000
        streetIndex = 0
        curClosestIndex = 0
        if index != 0:
            data = line.split(',')
            graf_lat = float(data[0])
            graf_lon = float(data[1])
            for street in allStreetCoords:
                for coord in street:
                    if(len(coord) != 0):
                        distance = abs(graf_lat - coord[0]) + abs(graf_lon - coord[1])
                        if(distance < curClosest):
                            curClosest = distance
                            curClosestIndex = streetIndex
                streetIndex += 1
            streetGrafCounts[curClosestIndex] += int(data[2])
        index += 1
    return streetGrafCounts

def main():
    street_data = ""
    intersection_data = ""
    public_street_file = open("./public_streets.json", "r")
    intersection_file = open("./street_intersections.json", "r")
    intersectCount = 0

    for line in intersection_file:
        intersection_data = intersection_data + line

    for line in public_street_file:
        street_data = street_data + line

    streetObj = json.loads(street_data)
    intersectionObj = json.loads(intersection_data)

    streetNames = set()

    allStreetCoords = []
    index = 0

    WriteableJson = open("streetsections.json", "w")

    StreetsByHblock = {}
    index = 0
    for feature in streetObj["features"]:
        index +=1
        try:
            if(StreetsByHblock[feature["properties"]["HBLOCK"]] != None):
                StreetsByHblock[feature["properties"]["HBLOCK"]]["geometry"]["coordinates"] = StreetsByHblock[feature["properties"]["HBLOCK"]]["geometry"]["coordinates"] + feature["geometry"]["coordinates"]
            else:
                print("In try but missed conditional")
        except:
            StreetsByHblock[feature["properties"]["HBLOCK"]] = feature
        
    index = 0
    prog = 0
    print_progress(0, len(streetObj["features"]), "Parsing Data")
    hblocks = []
    for feature in streetObj["features"]:
        # feature = StreetsByHblock[feature]
        current_street = []
        print_progress(prog, len(streetObj["features"]), "Parsing Data")
        prog+=1
        for coord in feature["geometry"]["coordinates"]:
            coordLatLon = []
            try:
                lat = utm.to_latlon(coord[0], coord[1], 10, "N")[0]
                lon = utm.to_latlon(coord[0], coord[1], 10, "N")[1]

                coordLatLon = [lat, lon]
            except:
                print(coord, " exception found")

            current_street.append(coordLatLon)

            for intersect in intersectionObj["features"]:
                intersect = intersect["geometry"]["coordinates"]

                if len(current_street) > 1 and coord[0] == intersect[0] and coord[1] == intersect[1]:
                    allStreetCoords.append(current_street)
                    # hblocks.append(feature["properties"]["HBLOCK"])
                    current_street = []
                    index += 1

        allStreetCoords.append(current_street)
        # hblocks.append(feature["properties"]["HBLOCK"])
        index += 1
    

    streetCrimeCounts = [0] * len(allStreetCoords)
    streetGrafCounts = [0] * len(allStreetCoords)
    
    streetGrafCounts = countGraffiti(allStreetCoords)
    streetCrimeCounts = countCrime(allStreetCoords)


    # crimeDataCsv = open("Crime_Data.csv", "r")
    # streetCrimeCounts = [0] * len(allStreetCoords)

    # totalCrimes = 0
    
    # for line in crimeDataCsv:
    #     totalCrimes += 1

    # crimeDataCsv.close()
    # crimeDataCsv = open("Crime_Data.csv", "r")

    # index = 0
    # print_progress(index, totalCrimes, "Counting Crimes")
    # for line in crimeDataCsv:
    #     print_progress(index, totalCrimes, "Counting Crimes" )
    #     curClosest = 10000
    #     streetIndex = 0
    #     curClosestIndex = 0
    #     if index != 0:
    #         data = line.split(',')
    #         crime_lat = float(data[0])
    #         crime_lon = float(data[1])
    #         for street in allStreetCoords:
    #             for coord in street:
    #                 # print(coord)
    #                 if(len(coord) != 0):
    #                     distance = abs(crime_lat - coord[0]) + abs(crime_lon - coord[1])
    #                     if(distance < curClosest):
    #                         # print(distance)
    #                         curClosest = distance
    #                         curClosestIndex = streetIndex
    #             streetIndex+=1
    #         streetCrimeCounts[curClosestIndex] += 1
    #     index += 1


    # index = 0

    # graffitiFile = open("graffiti.csv", "r")
    # totalGraffitiCount = 0
    # for line in graffitiFile:
    #     totalGraffitiCount += 1

    # graffitiFile.close()
    # graffitiFile = open("graffiti.csv", "r")
    # streetGrafCounts = [0] * len(allStreetCoords)
    
    # for line in graffitiFile:
    #     print_progress(index, totalGraffitiCount, "Counting Graffiti")
    #     curClosest = 10000
    #     streetIndex = 0
    #     curClosestIndex = 0
    #     if index != 0:
    #         data = line.split(',')
    #         graf_lat = float(data[0])
    #         graf_lon = float(data[1])
    #         for street in allStreetCoords:
    #             for coord in street:
    #                 if(len(coord) != 0):
    #                     distance = abs(graf_lat - coord[0]) + abs(graf_lon - coord[1])
    #                     if(distance < curClosest):
    #                         curClosest = distance
    #                         curClosestIndex = streetIndex
    #             streetIndex += 1
    #         streetGrafCounts[curClosestIndex] += data[2]
    #     index += 1


    # for street in allStreetCoords:
    #     print(street)

    # Dont use csv use JSON instead

    print(streetCrimeCounts)

    write_data = {}
    index = 0

    write_data["type"] = "FeatureCollection"
    write_data["name"] = "Publilc Streets (Parsed)"
    write_data["crs"] = {
		"type" : "name",
		"properties" : {
			"name" : "EPSG:26910"
		}
	}

    write_data["features"] = []


    for street in allStreetCoords:
        if len(street) == 0:
            # print("excluding")
            b=1
        else:
            # print(street)

            for coord in street:
                print(coord)
                if(len(coord) == 2):
                    write_data["features"].append( {
                        "type" : "Feature",
                        "geometry":{
                            "coordinates": street,
                            "type": "LineString"
                        },
                        "properties": {
                            "category": random.randint(0,10),
                            "length": len(street),
                            "crimes": streetCrimeCounts[index],
                            "graffiti": streetGrafCounts[index]
                        }
                    })
        index +=1


    # for feature in streetObj["features"]:
    #     feature["properties"]["category"] = random.randint(0,10)

    # write_data = streetObj

    writeable = json.dumps(write_data)
    WriteableJson.write(writeable)
    print("Found ", len(allStreetCoords), " unique streets")
    print("Found " , len(streetNames), " street names for ", len(streetObj["features"]))


main()



