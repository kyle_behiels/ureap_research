import csv

# Get and define test data

def getTestData():
    test_data_file = open("neighborhood_polygon_coords.csv", "r")
    test_data_reader = csv.reader(test_data_file)

    test_data_obj = {
        "fields": [],
        "rows": []
    }

    test_data_obj['fields'] = next(test_data_reader)
    for row in test_data_reader:
        test_data_obj['rows'].append(row)

    

    return test_data_obj



test_seg_one = [(1, 1), (5,4)]
test_seg_two = [(5, 1), (5,3)]


# CODE BORROWED FROM -> https://stackoverflow.com/a/18524383

def side(a,b,c):
    """ Returns a position of the point c relative to the line going through a and b
        Points a, b are expected to be different
    """
    d = (c[1]-a[1])*(b[0]-a[0]) - (b[1]-a[1])*(c[0]-a[0])
    return 1 if d > 0 else (-1 if d < 0 else 0)

def is_point_in_closed_segment(a, b, c):
    """ Returns True if c is inside closed segment, False otherwise.
        a, b, c are expected to be collinear
    """
    if a[0] < b[0]:
        return a[0] <= c[0] and c[0] <= b[0]
    if b[0] < a[0]:
        return b[0] <= c[0] and c[0] <= a[0]

    if a[1] < b[1]:
        return a[1] <= c[1] and c[1] <= b[1]
    if b[1] < a[1]:
        return b[1] <= c[1] and c[1] <= a[1]

    return a[0] == c[0] and a[1] == c[1]

#
def lines_intersect(a,b,c,d):
    """ Verifies if closed segments a, b, c, d do intersect.
    """
    if a == b:
        return a == c or a == d
    if c == d:
        return c == a or c == b

    s1 = side(a,b,c)
    s2 = side(a,b,d)

    # All points are collinear
    if s1 == 0 and s2 == 0:
        return \
            is_point_in_closed_segment(a, b, c) or is_point_in_closed_segment(a, b, d) or \
            is_point_in_closed_segment(c, d, a) or is_point_in_closed_segment(c, d, b)

    # No touching and on the same side
    if s1 and s1 == s2:
        return False

    s1 = side(c,d,a)
    s2 = side(c,d,b)

    # No touching and on the same side
    if s1 and s1 == s2:
        return False

    return True

# END OF BORROWED CODE


def pointInPolygon(point, polygon):
    """ Checks if a given point is within a given polygon

    Parameters:
        point (float, float): A point made of x,y coordinates
        polygon [(float, float)...(float, float)]: List of ordered points that make up the edge of a polygon

    Returns:
        pointInPolygon(Bool): True if the point is inside the polygon, else false

    """
    print(polygon)
    print("fio")



def main():
    testObj = getTestData()
    interiorPoint = (-123.09092, 49.21936)
    pointInPolygon(interiorPoint, testObj["rows"])
    print("test")



main()