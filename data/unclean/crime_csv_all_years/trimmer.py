import csv

dataFile = open("crime_csv_all_years.csv", "r")
csvFile = csv.reader(dataFile)

fields = next(csvFile)

rows = []

print(fields)

for row in csvFile:
    rows.append(row)

i = 0

parsed_rows = []

for row in rows:
    cur_row = []
    for val in row:
        if val == '':
            cur_row.append("null")
        else:
            cur_row.append(val.replace('\"', '').replace("\'", ''))
        

    i += 1
    
    parsed_rows.append(cur_row)

writeFile = open("crime_csv_all_years_clean.csv", "w")

csvWriter = csv.writer(writeFile)

csvWriter.writerow(fields)

for row in parsed_rows:
    csvWriter.writerow(row)

writeFile.close()
dataFile.close()